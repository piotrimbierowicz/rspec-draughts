RSpec Tutorial
==============

Hello. This few steps will help you to start using RSpec with rails applications.

Story
-----
We will use Checkers game as an example for testing. Code is simple. It has two models: Board and Piece. Board stores pieces in array, we don't use database storage.

Task 1.1
---------
Our two models already has small parts of code. Please read contents of app/models/board.rb and app/models/piece.rb.

Task 1.2
---------
...