class Piece < ActiveRecord::Base

  attr_accessor :board, :color, :piece_type

  COLORS = [Black = 'black', White = 'white']
  PIECE_TYPES = [Queen = 'queen', Man = 'man']

  ##
  # moves piece to selected coordinates
  # throws exception if move is illegal
  def move_to(x, y)

  end

  ##
  # returns true if move is legal
  # otherwise returns false
  def can_move_to?(x, y)

  end

end
