class Board < ActiveRecord::Base

  attr_accessor :pieces, :size

  ##
  # returns difference between number of white and black pieces
  # score is > 0 when white wins
  # score is < 0 when black wins
  # score is = 0 when draw
  def score

  end

  ##
  # returns piece standing on specified coordinates
  def get_piece(x, y)

  end

  def initialize(attrs = {})
    super
    self.pieces = {}
    initialize_pieces
  end


  ##
  # fills board with black and white pieces
  def initialize_pieces
    for i in (1..self.size)
      for j in (1..self.size)
        pieces[[i,j]] = nil
      end
    end
  end

end
