require 'rails_helper'

RSpec.describe Board, :type => :model do

  describe 'initialization' do

    it 'has size field' do
      b = Board.new(:size => 8)
      expect(b.size).to eq 8
    end

    it 'is filled by pieces' do
      b = Board.new(:size => 8)
      expect(b.pieces.size).to eq 8*8
    end

  end


end
